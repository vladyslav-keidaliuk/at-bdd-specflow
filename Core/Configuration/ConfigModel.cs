﻿namespace Core.Configuration;

public class ConfigModel
{
    public string? Browser { get; set; }
    public string? Url { get; set; }
    public string? DocumentName { get; set; }
    public bool IsHeadlessModeOn { get; set; }
}