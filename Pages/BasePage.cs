﻿using Core;
using OpenQA.Selenium;

namespace Pages;

public class BasePage
{
    protected readonly By HeaderTitle = By.XPath("//h1");
    protected SeleniumWebDriver DriverManager;

    public BasePage(SeleniumWebDriver driverManager)
    {
        DriverManager = driverManager;
    }

    public string GetHeader()
    {
        var element = DriverManager.FindElements(HeaderTitle).FirstOrDefault();
        if (element != null)
        {
            return element.Text;
        }
        throw new NoSuchElementException();
    }
}