﻿using Core;
using OpenQA.Selenium;

namespace Pages;

public class GuidePage : BasePage
{
    private readonly By _searchBar = By.XPath("//form/input[@type='text']");
    private readonly By _searchBarPopup = By.XPath("//input[@class='search__outer__input']");

    public GuidePage(SeleniumWebDriver driver) : base(driver) { }

    public void OpenSearchPopup()
    {
        DriverManager.FindElement(_searchBar).Click();
    }

    public void Search(string keyword)
    {
        DriverManager.FindElement(_searchBarPopup).SendKeys(keyword + Keys.Enter);
    }
}