﻿using Core;
using OpenQA.Selenium;

namespace Pages;

public class HomePage : BasePage
{
    private readonly By _docsLink = By.LinkText("Docs");
    private readonly By _specFlowLink = By.LinkText("SpecFlow");

    public HomePage(SeleniumWebDriver driver) : base(driver) { }

    public void HoverOverDocsLink()
    {
        var element = DriverManager.FindElement(_docsLink);
        DriverManager.HoverOverElement(element);
    }

    public void ClickOnSpecFlowFromList()
    {
        DriverManager.FindElement(_specFlowLink).Click();
    }
}