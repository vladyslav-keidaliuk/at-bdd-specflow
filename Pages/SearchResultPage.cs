﻿using Core;
using OpenQA.Selenium;

namespace Pages
{
    public class SearchResultPage : BasePage
    {
        private readonly By _searchResultItems = By.XPath("//ul[@class='search']/li/a");
        private readonly By _searchResultSummary = By.XPath("//p[@class='search-summary']");
        public SearchResultPage(SeleniumWebDriver driver) : base(driver) { }

        public void ChooseResult(string keyword)
        { 
            DriverManager.WaitForCondition(() => DriverManager.FindElement(_searchResultSummary).Displayed);

            var element = DriverManager
                .FindElements(_searchResultItems)
                .FirstOrDefault(el => el.Text.Contains(keyword, StringComparison.OrdinalIgnoreCase));

            if (element != null)
            {
                element.Click();
            }
            else
            {
                throw new ArgumentNullException(nameof(element), $"There is no results with '{keyword}'");
            }
        }
    }
}
