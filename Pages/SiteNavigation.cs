﻿using Core.Configuration;
using Core;

namespace Pages;

public class SiteNavigation : BasePage
{
    public SiteNavigation(SeleniumWebDriver driverManager) : base(driverManager) { }

    public static void GoToSpecFlowDotCom(SeleniumWebDriver driver)
    {
        driver.GoToUrl(Config.Model.Url);
    }
}