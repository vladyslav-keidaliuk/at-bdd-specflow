# .NET BDD Frameworks: Specflow

## Task 1.
Using Gherkin syntax and keywords, write the following script. Use Visual Studio or a text editor: 
 
1. Open https://specflow.org/ 
2. Hover to the Docs menu item 
3. Select SpecFlow 
4. Click on the ‘search docs’ field 
5. In the popup window, enter the text ‘Installation’ 
6. Select the ‘Installation’ result 
7. Check that the page titled ‘Installation’ opens 

## Task 2.
Automate a test from the previous practical task using SpecFlow. As a basis for the framework, you can take the project by the link:

https://github.com/kzadorozhnaya/specflow-demo  
