﻿Feature: GuideSearch

@searchFunctionality
Scenario: FindGuideThroughSearchForm
	Given I open official SpecFlow web site
	When  I hover the Docs menu item
	And I select SpecFlow from list
	And I click on the 'search docs' field
	And I search for 'Installation'
	And I select the 'Installation' result
	Then Page with title 'Installation' should be open
