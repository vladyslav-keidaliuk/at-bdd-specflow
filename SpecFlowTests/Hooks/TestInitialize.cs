﻿using Core;
using TechTalk.SpecFlow;
using static Core.SeleniumWebDriver;

namespace SpecFlowTests.Hooks
{
    [Binding]
    public sealed class TestInitialize
    {
        [BeforeScenario("@searchFunctionality")]
        public void BeforeScenarioWithTag()
        {
            NativeDriver.Driver.Manage().Window.Maximize();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            DriverManager.QuitDriver();
        }
    }
}