﻿using NUnit.Framework;
using Pages;
using static Core.SeleniumWebDriver;

namespace SpecFlowTests.Steps;

[Binding]
public sealed class NavigationStepDefinitions
{
    private readonly GuidePage _guidePage;
    private readonly HomePage _homePage;
    private readonly SearchResultPage _searchResultPage;

    public NavigationStepDefinitions()
    {
        _homePage = new HomePage(NativeDriver);
        _guidePage = new GuidePage(NativeDriver);
        _searchResultPage = new SearchResultPage(NativeDriver);
    }

    [Given(@"I open official SpecFlow web site")]
    public void OpenOfficialSpecFlowWebSite()
    {
        SiteNavigation.GoToSpecFlowDotCom(NativeDriver);
    }

    [When(@"I hover the Docs menu item")]
    public void HoverOverDocsLink()
    {
        _homePage.HoverOverDocsLink();
    }

    [When(@"I select SpecFlow from list")]
    public void ClickOnSpecFlowFromListAtTheMainMenu()
    {
        _homePage.ClickOnSpecFlowFromList();
    }

    [When(@"I click on the 'search docs' field")]
    public void ClickSearchField()
    {
        _guidePage.OpenSearchPopup();
    }

    [When(@"I search for '([^']*)'")]
    public void SearchFor(string search)
    {
        _guidePage.Search(search);
    }

    [When(@"I select the '([^']*)' result")]
    public void OpenSearchResult(string keyword)
    {
        _searchResultPage.ChooseResult(keyword);
    }

    [Then(@"Page with title '([^']*)' should be open")]
    public void PageWithTitleShouldBeOpen(string title)
    {
        Assert.That(_homePage.GetHeader(), Is.EqualTo(title).IgnoreCase);
    }
}